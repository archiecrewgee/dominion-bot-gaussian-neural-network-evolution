import Deck_Class as deck
import CONFIG

class Player():
    def __init__(self):
        self.hand = []
        self.playArea = []
        self.drawDeck = deck.Deck()
        self.discardDeck = deck.Deck()

        self.actionPoints = 0
        self.buyPoints = 0
        self.temporaryGold = 0

    def setup_turn(self):
        self.actionPoints = 1
        self.buyPoints = 1
        self.temporaryGold = 0

        self.draw()

    # ACTIONS
    # - end turn
    # - action card
    # - purchase
    def end_turn(self):
        self.discardDeck.add(self.hand)         # add hand to discard pile
        self.discardDeck.add(self.playArea)     # add all cards in play area to discard pile

        self.hand = []                          # clear hand
        self.playArea = []                      # clear play area

        self.draw()                             # draw five new cards
        return

    def action(self, cardName):
        # sort later
        # find card in hand
        for i in range(len(self.hand)):
            if self.hand[i].name == cardName:
                handIndex = i
                break
        # perform action
        #print("Actioning card:", cardName, "| hand index:", handIndex)
        liveCard = self.hand.pop(handIndex)             # remove card from hand
        self.playArea.append(liveCard)                  # add to play area
        liveCard.action(player=self)                    # action card

        self.actionPoints += liveCard.actionChange      # update action points
        self.buyPoints += liveCard.buyChange            # update buy points
        return

    def purchase(self, card):
        self.discardDeck.add([card])                # add purchased card to discard deck
        self.make_optimum_purchase(card.cost)       # finds optimum purchase, removes the cards used and updates the temp gold

        self.buyPoints -= 1                         # updates buy points

    def make_optimum_purchase(self, cost):
        # create target (gold must total greater than this)
        target = cost - self.temporaryGold

        # find all gold cards in hand and add to list, along with their index in hand
        handPurchasePower = []
        indexPurchaseList = []
        for i in range(len(self.hand)):
            if self.hand[i].goldChange != 0:
                indexPurchaseList.append(i)
                handPurchasePower.append(self.hand[i].goldChange)

        # sort these items with an index based bubble sort
        handPurchasePower, indexPurchaseList = self.index_bubble_sort(handPurchasePower, indexPurchaseList)

        # find optimum combinations
        for i in range(len(handPurchasePower)):
            if (sum(handPurchasePower[0:i+1]) >= target):
                # once target reached play cards
                # update temporary gold
                if (self.temporaryGold >  cost - sum(handPurchasePower[0:i+1])):
                    self.temporaryGold -= (cost - sum(handPurchasePower[0:i+1]))
                # remove payment cards
                indexPurchaseList = self.bubble_sort(indexPurchaseList[0:i+1])
                for i in range(len(indexPurchaseList)): self.play_card_without_action(indexPurchaseList[len(indexPurchaseList) - 1 - i])
                break



    def play_card_without_action(self, index):
        self.playArea.append(self.hand.pop(index))

    def bubble_sort(self, list):
        dummyValue = 0
        endOffset = 0
        while True:
            swaps = 0
            endOffset += 1
            for i in range(len(list) - endOffset):
                if list[i] > list[i+1]:
                    dummyValue = list[i]
                    list[i] = list[i+1]
                    list[i+1] = dummyValue

                    swaps += 1
            if swaps == 0: break
        return list

    def index_bubble_sort(self, list, index):
        dummyValue = [0, 0]
        endOffset = 0
        while True:
            swaps = 0
            endOffset += 1
            for i in range(len(list) - endOffset):
                if list[i] > list[i+1]:
                    dummyValue = [list[i], index[i]]
                    list[i] = list[i+1]
                    index[i] = index[i+1]
                    list[i+1] = dummyValue[0]
                    index[i+1] = dummyValue[1]

                    swaps += 1
            if swaps == 0: break
        return list, index

    def draw(self):
        while len(self.hand) < 5:
            self.draw_one()

    def draw_one(self):
        if len(self.drawDeck.contents) == 0:                # if draw deck is empty...
            self.discardDeck.shuffle()                      # shuffle discard pile
            self.drawDeck.add(self.discardDeck.contents)    # copy discard deck to draw deck
            self.discardDeck.clear()                        # empty contents of discard deck

        # perform check again as discard pile may have been empty as all cards are in play
        if len(self.drawDeck.contents) != 0:
            self.hand.append(self.drawDeck.draw())          # add top card from draw deck to hand

    def find_purchasing_power(self):
        return self.temporaryGold + sum([self.hand[i].goldChange for i in range(len(self.hand))])

    def display_hand(self):
        display = ""
        for i in range(len(self.hand)):
            display += self.hand[i].name + ", "
        print("Player hand |", display)

    def display_all(self):
        print("---- PLAYER ---- AP:", self.actionPoints, "| BP:", self.buyPoints, " | TG:", self.temporaryGold)
        self.display_hand()
        print("Play area:")
        print([x.name for x in self.playArea])
        print("Draw deck:")
        self.drawDeck.display()
        print("Discard deck:")
        self.discardDeck.display()

    def total_victory_points(self):
        sum = 0
        totalCards = self.hand + self.playArea + self.drawDeck.contents + self.discardDeck.contents
        for card in totalCards:
            sum += card.victoryPoints
        return sum

    def hand_count(self):
        array = [0 for x in range(16)]
        for card in self.hand:
            if card.name in CONFIG.CARDLIST:
                array[CONFIG.CARDLIST.index(card.name)] += 1
        return array

    def deck_count(self):
        array = [0 for x in range(16)]
        for card in self.drawDeck.contents:
            if card.name in CONFIG.CARDLIST:
                array[CONFIG.CARDLIST.index(card.name)] += 1
        for card in self.discardDeck.contents:
            if card.name in CONFIG.CARDLIST:
                array[CONFIG.CARDLIST.index(card.name)] += 1
        for card in self.hand:
            if card.name in CONFIG.CARDLIST:
                array[CONFIG.CARDLIST.index(card.name)] += 1
        for card in self.playArea:
            if card.name in CONFIG.CARDLIST:
                array[CONFIG.CARDLIST.index(card.name)] += 1
        return array