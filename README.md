# Dominion Bot Gaussian Neural Network Evolution

The following is an exploration of an AI for playing the baord game Dominion. This acts as a backdrop to attempting to train a neural network with Gaussian variation in weights.

Some basic weights are provided although the bot can be retrained by modifying the `main.py` file. 

Training is performed with rounds of N agents playing against one-another followed by a selection process based on fitness. Fitness of the agents is zero-sum.

These elements form an evolutionary algorithm. Complex behaviour can begin to be observed after many itterations with basic strategies of juggling pick-up and play cards to extend the number of victory points collected. The goal was to observe simultainious short- and long-term strategies although the lack of training time, size of neural network and incomplete ruleset mean the latter was not seen.

Note that most basic rules of Dominion are implemented but not all card actions are covered. 

## Setup

From repository top level:

 1. Setup virtual environment: `python -m venv .venv`
 2. Install requirements: `.\.venv\Scripts\python -m pip install -r requirements.txt`
 3. Run program: `.\.venv\Scripts\python main.py`

Modify the `main.py` file to switch between training and playng against the AI.
Modify the `CONFIG.py` file to change game and AI parameters. 