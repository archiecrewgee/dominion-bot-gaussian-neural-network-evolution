import CONFIG
import Deck_Class as deck
import Player_Class as player
import Card_Store as cards

class Dominion_Game:
    def __init__(self):
        self.playerCount = 0
        self.players = []
        self.supplyDecks = []
        self.turn = 0

        self.kingdomCardsDict = {}

    def setup(self, treasureCards, victoryCards, kingdomCards, playerCount_):
        #print("Begin setup | Player count:", playerCount_)
        self.playerCount = playerCount_

        #print("Adding Treasure cards")
        self.add_deck(treasureCards[0], 60)         # copper
        self.add_deck(treasureCards[1], 40)         # silver
        self.add_deck(treasureCards[2], 30)         # gold
        #print("Adding victory cards")
        self.add_card_decks(victoryCards, 8 + ((self.playerCount > 2) * 4)) # Estate, Duchy, Province
        #print("Adding kingdom cards")
        self.add_card_decks(kingdomCards, 10)

        displayText = ""
        for i in range(len(self.supplyDecks)): displayText += "\n" + str(i) + " | " + self.supplyDecks[i].contents[0].name + " | " + str(len(self.supplyDecks[i].contents))
        #print("Supply decks:(", len(self.supplyDecks), ")", displayText)

        #print("Defining kingdom cards")
        self.define_kingdom_cards(kingdomCards)

        #print("Initiating players")
        self.initiate_players()

    def initiate_players(self):
        for i in range(self.playerCount):
            dummyListOfCards = []
            dummyPlayer = player.Player()

            dummyListOfCards.extend(self.supplyDecks[0].multidraw(7))
            dummyListOfCards.extend(self.supplyDecks[3].multidraw(3))
            dummyPlayer.discardDeck.add(dummyListOfCards)

            dummyPlayer.draw()
            self.players.append(dummyPlayer)

        #print(self.playerCount, "players initiated")
        """for i in range(self.playerCount):
            print("\nPlayer", i)
            self.players[i].display_all()"""

    def action_turn(self, decision, playerNumber):
        kingdomCards = [cards.CellarMasterCard,
                        cards.MarketMasterCard,
                        cards.MerchantMasterCard,
                        cards.MilitiaMasterCard,
                        cards.MineMasterCard,
                        cards.MoatMasterCard,
                        cards.RemodelMasterCard,
                        cards.SmithyMasterCard,
                        cards.VillageMasterCard,
                        cards.WorkshopMasterCard]
        # end turn
        if (decision == CONFIG.ENDTURNINDEX):
            self.players[playerNumber].end_turn()
            return True
        # action card
        elif (decision < CONFIG.KINGDOMCARDS):
            self.players[playerNumber].action(kingdomCards[decision].name)
        # purchase card
        else:
            self.players[playerNumber].purchase(self.supplyDecks[decision - CONFIG.KINGDOMCARDS].draw())


        # play card .action
        # purchase - figure out optimum purchasing combo
        # return false
        # end turn - return true
        return False

    def define_kingdom_cards(self, cards):
        for i in range(len(cards)): self.kingdomCardsDict[cards[i].name] = i
        #print(self.kingdomCardsDict)

    def generate_possibility_filter(self, playerNumber):
        #print("Generating possibility filter for player", playerNumber)

        dummyPlayer = self.players[playerNumber]
        # possibility filter = [0->9 play kingdom card][10->26 purchase card][27 end turn]
        # define initial filter
        out = [False for i in range(CONFIG.ENDTURNINDEX + 1)]
        out[CONFIG.ENDTURNINDEX] = True

        # define playable cards
        if (dummyPlayer.actionPoints > 0):
            for card in dummyPlayer.hand:
                if card.name in self.kingdomCardsDict: out[self.kingdomCardsDict[card.name]] = 1

        # define cards that can be purchased
        if (dummyPlayer.buyPoints > 0):
            gold = dummyPlayer.find_purchasing_power()              # define player purchasing power
            for i in range(CONFIG.PURCHASABLECARDS):
                if (len(self.supplyDecks[i].contents) != 0):
                    out[i+CONFIG.KINGDOMCARDS] = (gold >= self.supplyDecks[i].contents[-1].cost)  # define what cards the player can purchase

        # return possibility filter
        #print("POSSIBILITY FILTER FOR PLAYER", playerNumber, " | ", out)
        return out

    def display_readable_possibility_filter(self, filter):
        kingdomCards = [cards.CellarMasterCard,
                        cards.MarketMasterCard,
                        cards.MerchantMasterCard,
                        cards.MilitiaMasterCard,
                        cards.MineMasterCard,
                        cards.MoatMasterCard,
                        cards.RemodelMasterCard,
                        cards.SmithyMasterCard,
                        cards.VillageMasterCard,
                        cards.WorkshopMasterCard]
        print("Can play:")
        for i in range(CONFIG.KINGDOMCARDS):
            if filter[i]: print(i, "|", kingdomCards[i].name)
        print("Can purchase:")
        for i in range(CONFIG.KINGDOMCARDS, CONFIG.ENDTURNINDEX):
            if filter[i]: print(i, "|", self.supplyDecks[i-CONFIG.KINGDOMCARDS].contents[-1].name)


    def end_state_reached(self):
        if len(self.supplyDecks[5].contents) == 0: return True
        return False

    def add_deck(self, card, count):
        #print("Creating", card.name, "deck for supply pile | Size:", count)
        dummyDeck = deck.Deck()
        dummyDeck.add([card for x in range(count)])
        self.supplyDecks.append(dummyDeck)

    def add_card_decks(self, cards, deckSize):
        for i in range(len(cards)): self.add_deck(cards[i], deckSize)

