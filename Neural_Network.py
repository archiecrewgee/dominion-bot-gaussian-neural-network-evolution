import numpy as np
import math
import random


def add_negative_offset(matrix):
    return np.hstack((matrix, -np.ones((np.shape(matrix)[0], 1))))

def random_weights_between_layers(sizeA, sizeB):
    return (np.random.rand(sizeA + 1, sizeB) - 0.5) / 2 * np.sqrt(sizeA)


def generate_guassian_array(values, mu, varience):
    array = np.arange(values) - (values / 2)
    array = np.exp(-np.square(array-mu) / (2 * np.square(varience))) / (varience * np.sqrt(2 * math.pi))
    #array = 1 - array
    array = (array - np.min(array)) / (np.max(array) - np.min(array))
    array[int(values/2):] = -array[int(values/2):]
    return array

def generate_new_weights(w, n, modificationArray):
    #print("GENERATING NEW WEIGHTS", np.shape(np.tile(w, (n, 1))))
    #print("Weight sizes:", np.shape(w[0]), ",", np.shape(w[1]), ",", np.shape(w[2]))
    np.random.seed()

    out = np.tile(w, (n,1))
    for i_network in range(1, n):
        #print("Updating network", i_network)
        for i_update in range(len(w)):
            dummyRandomArray = modificationArray[np.random.randint(0, len(modificationArray), np.shape(w[i_update]))]
            out[i_network][i_update] = out[i_network][i_update] + dummyRandomArray

    return out

# GUASSIAN EVOLUTION
# input = [[in values]]
# output = [[out values]]
# scoring(some measure of victory)
# generate N different perceptrons
# run all until end state
# Number of evolutions per perceptron (E) = N / total score * perception score
# apply guassian evolution filter for weight layers E times
# repeat

class guassian_evolution():
    # Initiate function
    def init(self, sizeArray, numberOfNetworks, evolutionFactor):
        self.numberOfNetworks = numberOfNetworks
        self.evolutionArray = generate_guassian_array(20, 0.0, 10) * evolutionFactor

        inputLayerSize = sizeArray[0]
        firstLayerSize = sizeArray[1]
        secondLayerSize = sizeArray[2]
        outputLayerSize = sizeArray[3]

        self.weights = np.ndarray((self.numberOfNetworks, 3), dtype=object)
        for i in range(self.numberOfNetworks):
            # Each network consists of three weights
            self.weights[i,0] = random_weights_between_layers(inputLayerSize, firstLayerSize)
            self.weights[i,1] = random_weights_between_layers(firstLayerSize, secondLayerSize)
            self.weights[i,2] = random_weights_between_layers(secondLayerSize, outputLayerSize)

        print("Initiated weights array | size", np.shape(self.weights))

    # Runs the program for a certain number of evolutions
    def train(self, numberOfEvolutions, run_function, evaluation_function):
        for i in range(numberOfEvolutions):
            # set all scores to 0
            scores = np.zeros((self.numberOfNetworks, 1))
            maxVP = 0
            # run through two player game with all combinations of players
            for i_p0 in range(self.numberOfNetworks):
                #print("Testing player", i_p0)
                for i_p1 in range(i_p0 + 1, self.numberOfNetworks):
                    # run game
                    dummyScores = run_function(state='train', weights=[self.weights[i_p0], self.weights[i_p1]])
                    if max(dummyScores) > maxVP: maxVP = max(dummyScores)
                    # modify scores
                    scores[i_p0] += evaluation_function(dummyScores[0], dummyScores[1])
                    scores[i_p1] += evaluation_function(dummyScores[1], dummyScores[0])

            self.weights = generate_new_weights(self.weights[np.argmax(scores)], self.numberOfNetworks, self.evolutionArray)

            if i % 1 == 0:
                print("Evolution", i, "| Best Performance", np.max(scores) , "of", self.numberOfNetworks - 1, "games played | Best victory point score", maxVP)

        #print("Best performance", np.max(scores))
        self.bestWeights = self.weights[np.argmax(scores)]
        return self.weights[np.argmax(scores)]

    def run_multilayer_perception(self, inputLayer, weightsArray):
        dummyHiddenLayer = inputLayer
        for array in weightsArray:
            dummyHiddenLayer = np.where(add_negative_offset(dummyHiddenLayer) @ array > 0, 1, 0)

        return dummyHiddenLayer
