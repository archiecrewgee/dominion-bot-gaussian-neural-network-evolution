import csv
import numpy as np

def save_weights(weights):
    for i in range(len(weights)):
        file = open("weights_" + str(i) + ".csv", 'w', newline='')
        writer = csv.writer(file)
        for i_ in range(np.shape(weights[i])[0]):
            writer.writerow(weights[i][i_,:])
        file.close()

def load_weights(n):
    out = np.zeros((n), dtype=object)
    for i in range(n):
        file = open("weights_" + str(i) + ".csv", 'r', newline='')
        reader = csv.reader(file)
        dataList = np.float_(list(reader))
        out[i] = dataList
        print(np.shape(dataList))
    return out
