import Dominion_Game_Class as dominion
import numpy as np
import CONFIG

def make_decision(game, possibilityFilter):
    while True:
        userInput = input("Enter number of decision:  ")
        if userInput.isnumeric() and possibilityFilter[int(userInput)]: break

    return int(userInput)

def make_decision_with_perceptron(game, playerNumber, possibilityFilter, weights):
    inputArray = []
    inputArray.extend(game.players[playerNumber].hand_count())
    inputArray.extend(game.players[playerNumber].deck_count())
    inputArray.extend([game.players[playerNumber].find_purchasing_power()])
    inputArray.extend([game.players[playerNumber].actionPoints])
    inputArray.extend([game.players[playerNumber].buyPoints])
    for i in range(len(game.players)):
        if i == playerNumber: continue
        inputArray.extend(game.players[i].deck_count())

    output = run_multilayer_perception([inputArray], weights).flatten()
    max = - 1000
    maxIndex = CONFIG.ENDTURNINDEX
    for i in range(len(possibilityFilter)):
        if possibilityFilter[i] == False: continue
        if max < output[i]:
            max = output[i]
            maxIndex = i

    return maxIndex

def run_multilayer_perception(inputLayer, weightsArray):
    dummyHiddenLayer = inputLayer
    for i in range(len(weightsArray) - 1):
        dummyHiddenLayer = np.where(add_negative_offset(dummyHiddenLayer) @ weightsArray[i] > 0, 1, 0)

    return add_negative_offset(dummyHiddenLayer) @ weightsArray[-1]

def add_negative_offset(matrix):
    return np.hstack((matrix, -np.ones((np.shape(matrix)[0], 1))))