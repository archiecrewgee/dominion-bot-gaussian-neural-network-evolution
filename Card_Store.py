import Card_Class as card
import Card_Functions as fn

# Cards defined as:
#   - name
#   - cost
#   - victory points
#   - action change (starts at -1)
#   - buy change
#   - gold change
#   - action

# Treasure cards
CopperMasterCard = card.Card()
CopperMasterCard.define("Copper", 0, 0, 0, 1, 0, CopperMasterCard.action)

SilverMasterCard = card.Card()
SilverMasterCard.define("Silver", 3, 0, 0, 2, 0, SilverMasterCard.action)

GoldMasterCard = card.Card()
GoldMasterCard.define("Gold", 6, 0, 0, 3, 0, GoldMasterCard.action)

# Victory Cards
EstateMasterCard = card.Card()
EstateMasterCard.define("Estate", 2, 2, 0, 0, 0, EstateMasterCard.action)

DuchyMasterCard = card.Card()
DuchyMasterCard.define("Duchy", 5, 3, 0, 0, 0, DuchyMasterCard.action)

ProvinceMasterCard = card.Card()
ProvinceMasterCard.define("Province", 8, 6, 0, 0, 0, ProvinceMasterCard.action)

# Kingdom Cards
CellarMasterCard = card.Card()
CellarMasterCard.define("Cellar", 2, 0, 1, 0, 0, fn.cellar)

MarketMasterCard = card.Card()
MarketMasterCard.define("Market", 5, 0, 1, 0, 1, fn.market)

MerchantMasterCard = card.Card()
MerchantMasterCard.define("Merchant", 3, 0, 1, 0, 0, fn.merchant)

MilitiaMasterCard = card.Card()
MilitiaMasterCard.define("Militia", 4, 0, 0, 0, 0, fn.militia)

MineMasterCard = card.Card()
MineMasterCard.define("Mine", 5, 0, 0, 0, 0, fn.mine)

MoatMasterCard = card.Card()
MoatMasterCard.define("Moat", 2, 0, 0, 0, 0, fn.moat)

RemodelMasterCard = card.Card()
RemodelMasterCard.define("Remodel", 4, 0, 0, 0, 0, fn.remodel)

SmithyMasterCard = card.Card()
SmithyMasterCard.define("Smithy", 4, 0, 0, 0, 0, fn.smithy)

VillageMasterCard = card.Card()
VillageMasterCard.define("Village", 3, 0, 2, 0, 0, fn.village)

WorkshopMasterCard = card.Card()
WorkshopMasterCard.define("Workshop", 3, 0, 0, 0, 0, fn.workshop)





CellarMasterCard.display()
MarketMasterCard.display()
MerchantMasterCard.display()
MilitiaMasterCard.display()
MineMasterCard.display()
MoatMasterCard.display()
RemodelMasterCard.display()
SmithyMasterCard.display()
VillageMasterCard.display()
WorkshopMasterCard.display()

CopperMasterCard.display()
SilverMasterCard.display()
GoldMasterCard.display()

EstateMasterCard.display()
DuchyMasterCard.display()
ProvinceMasterCard.display()