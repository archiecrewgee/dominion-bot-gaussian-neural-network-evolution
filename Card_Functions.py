import Card_Store as cards
import Player_Class

def cellar(player=None):
    # Discard any number of cards, then draw that many
    # temp - discard and draw 1 - need to make this better
    if len(player.hand) != 0:
        player.discardDeck.add([player.hand[0]])
        del player.hand[0]
    player.draw_one()

    return


def market(player=None):
    # draw 1
    # +1 temp gold
    player.temporaryGold += 1
    player.draw_one()
    return


def merchant(player=None):
    # draw 1
    # The first time you play silver this turn +1 gold
    player.draw_one()
    # need to add silver stipulation
    return


def militia(player=None):
    # Each other player discards down to 3 cards in hand
    # +2 temp gold
    # need to add affecting other players ability ??
    player.temporaryGold += 2
    return


def mine(player=None):
    # You may trash a treasure from your hand. Gain a Treasure to your hand costing up to 3 more than it
    # temp - performing on first treasure found - not taking from decks
    upgradedTreasure = [cards.SilverMasterCard, cards.GoldMasterCard]
    for i in range(len(player.hand)):
        if player.hand[i].goldChange > 0 and player.hand[i].goldChange != 3:
            player.hand[i] = upgradedTreasure[player.hand[i].goldChange - 1]
            break
    return


def moat(player=None):
    # Draw 2
    # When another player plays an attack card, you may first reveal this from your hand to be unaffected by it
    # need to add counter / do in affecting other players thing
    player.draw_one()
    player.draw_one()
    return


def remodel(player=None):
    # Trash a card from your hand.
    # gain a card costing up to 2 more than it
    # not working for the time being
    return


def smithy(player=None):
    # draw 3
    player.draw_one()
    player.draw_one()
    player.draw_one()
    return


def village(player=None):
    # draw 1
    player.draw_one()
    return


def workshop(player=None):
    # gain a card costing up to 4
    # not working for the time being
    return