class Card:
    def __init__(self):
        self.name = ""
        self.cost = 0
        self.actionChange = -1
        self.goldChange = 0
        self.buyChange = 0
        self.victoryPoints = 0

    def action(self):
        return

    def define(self, name_, cost_, victoryPoints_, actionChange_, goldChange_, buyChange_, action_):
        self.name = name_
        self.cost = cost_
        self.actionChange += actionChange_
        self.goldChange = goldChange_
        self.buyChange = buyChange_
        self.action = action_
        self.victoryPoints = victoryPoints_

    def display(self):
        print("CARD", self.name, "| Cost:", self.cost, "| VP:", self.victoryPoints, "| ActionChange:", self.actionChange, "| GoldChange:", self.goldChange, "| BuyChange:", self.buyChange)


