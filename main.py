import CONFIG
import Dominion_Game_Class as dominion
import Card_Store as cards
import Decision_Maker as decision
from Neural_Network import guassian_evolution
import System_Interaction
import numpy as np


def run_full_game(weights=None, state='train', display=False):
    treasureCards = [cards.CopperMasterCard, cards.SilverMasterCard, cards.GoldMasterCard]
    victoryCards = [cards.EstateMasterCard, cards.DuchyMasterCard, cards.ProvinceMasterCard]
    kingdomCards = [cards.CellarMasterCard,
                    cards.MarketMasterCard,
                    cards.MerchantMasterCard,
                    cards.MilitiaMasterCard,
                    cards.MineMasterCard,
                    cards.MoatMasterCard,
                    cards.RemodelMasterCard,
                    cards.SmithyMasterCard,
                    cards.VillageMasterCard,
                    cards.WorkshopMasterCard]

    testGame = dominion.Dominion_Game()
    testGame.setup(treasureCards, victoryCards, kingdomCards, CONFIG.PLAYERCOUNT)

    # run game
    gameDepth = 0       # create depth variable
    while (gameDepth <= CONFIG.MAXGAMEDEPTH) and not(testGame.end_state_reached()):         # while below game depth and end state not reached
        turnDepth = 0                                                                   # reset turn depth
        playerNumber = gameDepth % CONFIG.PLAYERCOUNT                                   # define current player number
        testGame.players[playerNumber].setup_turn()                                     # initiate current for turn (reset action points etc.)

        if display: print("-------------------\nTURN:", gameDepth, "| Player:", playerNumber)
        # turn phase
        while (turnDepth <= CONFIG.MAXTURNDEPTH) and not(testGame.end_state_reached()):    # while below turn depth and end state not reached (? maybe change because itll apply to upper)
            moveFilter = testGame.generate_possibility_filter(playerNumber)             # create filter of possible moves
            if display:
                print("Turn move:", turnDepth)
                testGame.players[playerNumber].display_all()
                testGame.players[playerNumber].display_hand()
                testGame.display_readable_possibility_filter(moveFilter)                    # display possible moves in readable manner

            # determine which move to undertake - if network true uses the weights supplied
            if state == 'train':
                decisionNumber = decision.make_decision_with_perceptron(testGame, playerNumber, moveFilter, weights[playerNumber])
            elif state == 'play_against_AI':
                if playerNumber != 0:
                    decisionNumber = decision.make_decision(testGame, moveFilter)
                else:
                    decisionNumber = decision.make_decision_with_perceptron(testGame, playerNumber, moveFilter, weights)
            elif state == 'just_humans':
                decisionNumber = decision.make_decision(testGame, moveFilter)

            if testGame.action_turn(decisionNumber, playerNumber): break                # action this move | returns true if turn is over
            turnDepth += 1                                                              # increment turn depth
        gameDepth += 1                                                                  # increment game depth
    if display:
        print("---------- GAME END -----------\n---------- SCORE -----------")
        for i in range(CONFIG.PLAYERCOUNT):
            print("Player", i, " | ", testGame.players[i].total_victory_points())

    return [testGame.players[x].total_victory_points() for x in range(CONFIG.PLAYERCOUNT)]

def evaluate_scores(a, b):
    if a > b:  return 1
    if a == b: return 0
    return -1

def train_AI():
    sizeArray = [CONFIG.INPUTLAYERSIZE, CONFIG.HIDDENLAYERSIZE0, CONFIG.HIDDENLAYERSIZE1, CONFIG.OUTPUTLAYERSIZE]
    trainingFunction = guassian_evolution()
    trainingFunction.init(sizeArray, CONFIG.NUMBEROFNETWORKS, CONFIG.EVOLUTIONFACTOR)
    bestWeights = trainingFunction.train(CONFIG.NUMBEROFEVOLUTIONS, run_full_game, evaluate_scores)
    System_Interaction.save_weights(bestWeights)

def play_AI():
    weights = System_Interaction.load_weights(3)
    print(np.shape(weights))
    run_full_game(weights=weights, state='play_against_AI', display=True)

play_AI()
#train_AI()