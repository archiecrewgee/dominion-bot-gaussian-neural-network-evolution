import random

class Deck:
    def __init__(self):
        self.contents = []

    def add(self, listOfCards):
        self.contents.extend(listOfCards)
        return

    def size(self):
        return (len(self.contents))

    def draw(self):
        return (self.contents.pop(0))

    def multidraw(self, n):
        out = []
        for i in range(n): out.append(self.draw())
        return out

    def shuffle(self):
        dummyContents = []
        while self.size() != 0:
            dummyContents.append(self.contents.pop(random.randint(0, self.size() - 1)))
        self.contents = dummyContents

    def clear(self):
        self.contents = []

    def display(self):
        for i in range(self.size()):
            print(i, "|", self.contents[i].name)